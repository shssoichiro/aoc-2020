#[aoc(day5, part1)]
pub fn part1(input: &str) -> u32 {
    input.lines().map(calculate_seat_id).max().unwrap()
}

fn calculate_seat_id(pass: &str) -> u32 {
    let mut row_range = (0u32, 127u32);
    let mut col_range = (0u32, 7u32);
    for (i, c) in pass.chars().enumerate() {
        if i < 7 {
            // This represents the row calculation
            match c {
                'F' => {
                    row_range.1 = (row_range.1 + row_range.0) / 2;
                }
                'B' => {
                    row_range.0 = (row_range.1 + row_range.0 + 1) / 2;
                }
                _ => unreachable!(),
            }
        } else {
            // This represents the column calculation
            match c {
                'L' => {
                    col_range.1 = (col_range.1 + col_range.0) / 2;
                }
                'R' => {
                    col_range.0 = (col_range.1 + col_range.0 + 1) / 2;
                }
                _ => unreachable!(),
            }
        }
    }
    row_range.0 * 8 + col_range.0
}

#[test]
fn test_day5_part1() {
    assert_eq!(357, calculate_seat_id("FBFBBFFRLR"));
    assert_eq!(567, calculate_seat_id("BFFFBBFRRR"));
    assert_eq!(119, calculate_seat_id("FFFBBBFRRR"));
    assert_eq!(820, calculate_seat_id("BBFFBBFRLL"));
}

#[aoc(day5, part2)]
pub fn part2(input: &str) -> u32 {
    let mut taken_seats = input.lines().map(calculate_seat_id).collect::<Vec<_>>();
    taken_seats.sort_unstable();
    for i in 0..(taken_seats.len() - 1) {
        if taken_seats[i] + 1 != taken_seats[i + 1] {
            return taken_seats[i] + 1;
        }
    }
    unreachable!();
}
