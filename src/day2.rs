use nom::bytes::complete::{tag, take};
use nom::character::complete::{char, digit1, not_line_ending};
use nom::sequence::tuple;

#[derive(Debug, Clone, Copy)]
pub struct PasswordInput<'a> {
    pub rule_first_value: usize,
    pub rule_second_value: usize,
    pub target_char: char,
    pub password: &'a str,
}

impl<'a> PasswordInput<'a> {
    // And of course we can't use the trait because it doesn't like explicit lifetimes
    fn from_str(input: &'a str) -> Self {
        // The target pattern looks like this:
        // 1-3 a: abcde
        let raw_parsed = tuple::<_, _, nom::error::Error<&str>, _>((
            digit1,
            char('-'),
            digit1,
            char(' '),
            take(1usize),
            tag(": "),
            not_line_ending,
        ))(input)
        .unwrap()
        .1;
        PasswordInput {
            rule_first_value: raw_parsed.0.parse().unwrap(),
            rule_second_value: raw_parsed.2.parse().unwrap(),
            target_char: raw_parsed.4.chars().next().unwrap(),
            password: raw_parsed.6,
        }
    }
}

// Can't use the generator macro because it doesn't work with structs that have lifetimes...
pub fn generator(input: &str) -> impl Iterator<Item = PasswordInput> {
    input.lines().map(PasswordInput::from_str)
}

#[aoc(day2, part1)]
pub fn part1(input: &str) -> usize {
    let mut correct_count = 0;
    for rule in generator(input) {
        // We can do this because we know all the inputs are ASCII,
        // this is faster than the unicode-safe method
        //
        // Clippy tells us to use the `bytecount` crate because it's faster than doing `.iter().filter().count()`
        let target_char_count = bytecount::count(rule.password.as_bytes(), rule.target_char as u8);
        if target_char_count >= rule.rule_first_value && target_char_count <= rule.rule_second_value
        {
            correct_count += 1;
        }
    }
    correct_count
}

#[test]
fn test_day2_part1() {
    let input = "1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc";
    assert_eq!(2, part1(input));
}

#[aoc(day2, part2)]
pub fn part2(input: &str) -> usize {
    let mut correct_count = 0;
    for rule in generator(input) {
        // We can do this because we know all the inputs are ASCII
        let password = rule.password.as_bytes();
        if (password[rule.rule_first_value - 1] == rule.target_char as u8)
            ^ (password[rule.rule_second_value - 1] == rule.target_char as u8)
        {
            correct_count += 1;
        }
    }
    correct_count
}

#[test]
fn test_day2_part2() {
    let input = "1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc";
    assert_eq!(1, part2(input));
}
