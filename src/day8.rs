use std::collections::BTreeSet;
use std::str::FromStr;

#[derive(Debug, Clone, Copy)]
pub enum Operator {
    Nop,
    Acc,
    Jmp,
}

impl FromStr for Operator {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "nop" => Operator::Nop,
            "acc" => Operator::Acc,
            "jmp" => Operator::Jmp,
            _ => unimplemented!("Operator not supported"),
        })
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Command {
    pub operator: Operator,
    pub offset: i16,
}

#[aoc_generator(day8)]
pub fn parse_input(input: &str) -> Vec<Command> {
    input
        .lines()
        .map(|line| {
            let mut split = line.split(' ');
            Command {
                operator: Operator::from_str(&split.next().unwrap()).unwrap(),
                offset: split.next().unwrap().parse().unwrap(),
            }
        })
        .collect()
}

#[aoc(day8, part1)]
pub fn part1(input: &[Command]) -> i32 {
    let mut acc = 0i32;
    let mut row = 0;
    let mut visited = BTreeSet::new();
    // The loop will exit if we have already visited this row
    while visited.insert(row) {
        match input[row].operator {
            Operator::Nop => {
                row += 1;
            }
            Operator::Acc => {
                acc += input[row].offset as i32;
                row += 1;
            }
            Operator::Jmp => {
                if input[row].offset >= 0 {
                    row += input[row].offset as usize;
                } else {
                    row -= input[row].offset.abs() as usize;
                }
            }
        }
    }
    acc
}

#[aoc(day8, part2)]
pub fn part2(input: &[Command]) -> i32 {
    let mut loop_iter = 0;
    loop {
        let mut i = 0;
        let mut acc = 0i32;
        let mut row = 0;
        let mut visited = BTreeSet::new();
        // The loop will exit if we have already visited this row
        while visited.insert(row) {
            let operator = if i == loop_iter {
                match input[row].operator {
                    Operator::Nop => Operator::Jmp,
                    Operator::Jmp => Operator::Nop,
                    Operator::Acc => Operator::Acc,
                }
            } else {
                input[row].operator
            };
            i += 1;

            match operator {
                Operator::Nop => {
                    row += 1;
                }
                Operator::Acc => {
                    acc += input[row].offset as i32;
                    row += 1;
                }
                Operator::Jmp => {
                    if input[row].offset >= 0 {
                        row += input[row].offset as usize;
                    } else {
                        row -= input[row].offset.abs() as usize;
                    }
                }
            }
            if row == input.len() {
                return acc;
            }
        }
        loop_iter += 1;
    }
}

#[test]
pub fn test_day8_part1() {
    let input = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";
    assert_eq!(5, part1(&parse_input(input)));
}

#[test]
pub fn test_day8_part2() {
    let input = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";
    assert_eq!(8, part2(&parse_input(input)));
}
