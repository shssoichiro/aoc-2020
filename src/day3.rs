// This represents the singular rectangle of input data, without any repetition.
#[derive(Debug, Clone)]
pub struct MapData {
    pub width: usize,
    pub height: usize,
    // This is a Vec of columns, each of which will contain
    // a Vec of row indices which have a tree in that column.
    pub tree_coords: Vec<Vec<usize>>,
}

#[aoc_generator(day3)]
pub fn generator(input: &str) -> MapData {
    let mut width = 0;
    let mut height = 0;
    let mut tree_coords = Vec::new();
    for (y, row) in input.lines().enumerate() {
        if width == 0 {
            width = row.len();
            tree_coords = vec![Vec::new(); width];
        }
        height += 1;
        for (x, val) in row.chars().enumerate() {
            if val == '#' {
                tree_coords[x].push(y);
            }
        }
    }
    MapData {
        width,
        height,
        tree_coords,
    }
}

fn count_trees(input: &MapData, x_slope: usize, y_slope: usize) -> usize {
    let mut x = 0;
    let mut y = 0;
    let mut trees = 0;
    while y < input.height {
        x += x_slope;
        y += y_slope;
        if input.tree_coords[x % input.width].contains(&y) {
            trees += 1;
        }
    }
    trees
}

#[aoc(day3, part1)]
pub fn part1(input: &MapData) -> usize {
    count_trees(input, 3, 1)
}

#[test]
fn test_day3_part1() {
    let input = "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#";
    assert_eq!(7, part1(&generator(input)));
}

#[aoc(day3, part2)]
pub fn part2(input: &MapData) -> usize {
    let slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    slopes
        .iter()
        .map(|&(x_slope, y_slope)| count_trees(input, x_slope, y_slope))
        .product()
}

#[test]
fn test_day3_part2() {
    let input = "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#";
    assert_eq!(336, part2(&generator(input)));
}
