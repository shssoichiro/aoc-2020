use std::cmp::Ordering;

#[aoc_generator(day1)]
pub fn generator(input: &str) -> Vec<u32> {
    // Our solvers will optimize by assuming the input is sorted in ascending order
    let mut values = input
        .lines()
        .map(|value| value.parse().unwrap())
        .collect::<Vec<u32>>();
    values.sort_unstable();
    values
}

#[aoc(day1, part1)]
pub fn part1(input: &[u32]) -> u32 {
    for (i, &value1) in input.iter().enumerate() {
        for &value2 in input.iter().skip(i + 1) {
            let sum = value1 + value2;
            match sum.cmp(&2020) {
                Ordering::Greater => {
                    break;
                }
                Ordering::Equal => {
                    return value1 * value2;
                }
                Ordering::Less => (),
            }
        }
    }
    unreachable!()
}

#[aoc(day1, part2)]
pub fn part2(input: &[u32]) -> u32 {
    for (i, &value1) in input.iter().enumerate() {
        for (j, &value2) in input.iter().enumerate().skip(i + 1) {
            for &value3 in input.iter().skip(j + 1) {
                let sum = value1 + value2 + value3;
                match sum.cmp(&2020) {
                    Ordering::Greater => {
                        break;
                    }
                    Ordering::Equal => {
                        return value1 * value2 * value3;
                    }
                    Ordering::Less => (),
                }
            }
        }
    }
    unreachable!()
}

#[test]
fn test_day1_part1() {
    let input = "1721
979
366
299
675
1456";
    assert_eq!(1721 * 299, part1(&generator(input)));
}

#[test]
fn test_day1_part2() {
    let input = "1721
979
366
299
675
1456";
    assert_eq!(979 * 366 * 675, part2(&generator(input)));
}
