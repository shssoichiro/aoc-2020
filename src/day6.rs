use itertools::Itertools;

#[aoc(day6, part1)]
pub fn part1(input: &str) -> usize {
    input
        .split("\n\n")
        .map(|answers| {
            answers
                .chars()
                .filter(|c| c.is_ascii_alphabetic())
                .unique()
                .count()
        })
        .sum()
}

#[test]
fn test_day6_part1() {
    let input = "abc

a
b
c

ab
ac

a
a
a
a

b";
    assert_eq!(11, part1(input));
}

#[aoc(day6, part2)]
pub fn part2(input: &str) -> usize {
    input
        .split("\n\n")
        .map(|answers| {
            let mut individuals = answers
                .lines()
                .map(|line| line.chars().unique().collect::<Vec<char>>())
                .collect::<Vec<Vec<char>>>();
            // Put the shortest items at the front to decrease the number of lookups needed
            individuals.sort_unstable_by_key(|i| i.len());
            individuals[0]
                .iter()
                .filter(|answer| {
                    individuals
                        .iter()
                        .skip(1)
                        .all(|individual| individual.contains(answer))
                })
                .count()
        })
        .sum()
}

#[test]
fn test_day6_part2() {
    let input = "abc

a
b
c

ab
ac

a
a
a
a

b";
    assert_eq!(6, part2(input));
}
