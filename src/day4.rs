use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;

#[derive(Debug, Clone)]
pub struct PassportData<'a> {
    pub fields: HashMap<&'a str, &'a str>,
}

pub fn generator(input: &str) -> impl Iterator<Item = PassportData> {
    input.split("\n\n").map(|line| PassportData {
        fields: line
            .split_ascii_whitespace()
            .map(|field| {
                let mut split = field.split(':');
                (split.next().unwrap(), split.next().unwrap())
            })
            .collect(),
    })
}

#[aoc(day4, part1)]
pub fn part1(input: &str) -> usize {
    const REQUIRED_FIELDS: &[&str] = &["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];

    let passports = generator(input);
    passports
        .filter(|passport| {
            REQUIRED_FIELDS
                .iter()
                .all(|field| passport.fields.contains_key(field))
        })
        .count()
}

#[test]
fn test_day4_part1() {
    let input = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in";
    assert_eq!(2, part1(input));
}

#[aoc(day4, part2)]
pub fn part2(input: &str) -> usize {
    let passports = generator(input);
    passports
        .filter(|passport| {
            validate_byr(passport)
                && validate_iyr(passport)
                && validate_eyr(passport)
                && validate_hgt(passport)
                && validate_hcl(passport)
                && validate_ecl(passport)
                && validate_pid(passport)
        })
        .count()
}

fn validate_byr(passport: &PassportData) -> bool {
    passport
        .fields
        .get(&"byr")
        .and_then(|byr| byr.parse::<u16>().ok())
        .map(|byr| byr >= 1920 && byr <= 2002)
        .unwrap_or(false)
}

fn validate_iyr(passport: &PassportData) -> bool {
    passport
        .fields
        .get(&"iyr")
        .and_then(|iyr| iyr.parse::<u16>().ok())
        .map(|iyr| iyr >= 2010 && iyr <= 2020)
        .unwrap_or(false)
}

fn validate_eyr(passport: &PassportData) -> bool {
    passport
        .fields
        .get(&"eyr")
        .and_then(|eyr| eyr.parse::<u16>().ok())
        .map(|eyr| eyr >= 2020 && eyr <= 2030)
        .unwrap_or(false)
}

fn validate_hgt(passport: &PassportData) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^(\d{2,3})(cm|in)$").unwrap();
    }

    passport
        .fields
        .get(&"hgt")
        .and_then(|hgt| RE.captures(hgt))
        .map(|captures| {
            if &captures[2] == "cm" {
                let cm = captures[1].parse::<u16>().unwrap();
                return cm >= 150 && cm <= 193;
            } else if &captures[2] == "in" {
                let in_ = captures[1].parse::<u16>().unwrap();
                return in_ >= 59 && in_ <= 76;
            }
            false
        })
        .unwrap_or(false)
}

fn validate_hcl(passport: &PassportData) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
    }

    passport
        .fields
        .get(&"hcl")
        .map(|hcl| RE.is_match(hcl))
        .unwrap_or(false)
}

fn validate_ecl(passport: &PassportData) -> bool {
    const ALLOWED_VALUES: &[&str] = &["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];

    passport
        .fields
        .get(&"ecl")
        .map(|ecl| ALLOWED_VALUES.contains(ecl))
        .unwrap_or(false)
}

fn validate_pid(passport: &PassportData) -> bool {
    passport
        .fields
        .get(&"pid")
        .map(|pid| pid.len() == 9 && pid.chars().all(|c| c.is_ascii_digit()))
        .unwrap_or(false)
}

#[test]
fn test_day4_part2() {
    let input = "eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007";
    assert_eq!(0, part2(input));

    let input = "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719";
    assert_eq!(4, part2(input));
}
