use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{alpha1, char, digit1};
use nom::combinator::{map, recognize};
use nom::error::ParseError;
use nom::multi::separated_list1;
use nom::sequence::{terminated, tuple};
use nom::IResult;
use std::collections::HashSet;

#[derive(Debug, Clone)]
pub struct BagRule<'a> {
    pub bag_style: &'a str,
    pub contents: Vec<(u8, &'a str)>,
}

pub fn parse_bag_rules(input: &str) -> impl Iterator<Item = BagRule> {
    input.lines().map(|rule| {
        let (_, result) = tuple::<_, _, nom::error::Error<&str>, _>((
            bag_style,
            tag(" contain "),
            alt((
                map(tag("no other bags"), |_| Vec::new()),
                separated_list1(
                    tag(", "),
                    map(tuple((digit1, char(' '), bag_style)), |result| {
                        (result.0.parse::<u8>().unwrap(), result.2)
                    }),
                ),
            )),
        ))(rule)
        .unwrap();
        BagRule {
            bag_style: result.0,
            contents: result.2,
        }
    })
}

pub fn bag_style<'a, Error: ParseError<&'a str>>(
    input: &'a str,
) -> IResult<&'a str, &'a str, Error> {
    terminated(
        recognize(tuple((alpha1, char(' '), alpha1))),
        alt((tag(" bags"), tag(" bag"))),
    )(input)
}

#[aoc(day7, part1)]
pub fn part1(input: &str) -> usize {
    let rules = parse_bag_rules(input).collect::<Vec<_>>();
    recursive_bag_search(&rules, "shiny gold").len()
}

fn recursive_bag_search<'a>(rules: &'a [BagRule], target: &str) -> HashSet<&'a str> {
    // Our accumulator is going to be a HashSet as an easy way of removing duplicates
    let can_hold_target = rules
        .iter()
        .filter(|rule| {
            rule.contents
                .iter()
                .any(|content_rule| content_rule.1 == target)
        })
        .map(|rule| rule.bag_style)
        .collect::<HashSet<_>>();
    can_hold_target
        .iter()
        .fold(can_hold_target.clone(), |mut acc, target| {
            acc.extend(recursive_bag_search(rules, target).into_iter());
            acc
        })
}

#[aoc(day7, part2)]
pub fn part2(input: &str) -> usize {
    let rules = parse_bag_rules(input).collect::<Vec<_>>();
    // Here we subtract one to exclude the top-level bag,
    // because the question is how many bags are *inside* our shiny gold bag
    recursive_bag_counter(&rules, "shiny gold") - 1
}

fn recursive_bag_counter<'a>(rules: &'a [BagRule], target: &str) -> usize {
    let rule = rules.iter().find(|rule| rule.bag_style == target).unwrap();
    // Here we want to add one to ensure the top-level bag is included
    rule.contents
        .iter()
        .map(|target| target.0 as usize * recursive_bag_counter(rules, target.1))
        .sum::<usize>()
        + 1
}

#[test]
pub fn test_day7_part1() {
    let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";
    assert_eq!(4, part1(input));
}

#[test]
pub fn test_day7_part2() {
    let input = "shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.";
    assert_eq!(126, part2(input));
}
